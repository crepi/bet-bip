package com.infobip.campus.betbip.communication;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.infobip.campus.betbip.models.Game;
import com.infobip.campus.betbip.user.User;

public class Communicator {
	private static Communicator comm = null; 

	private String request;
	private String response;
	private WebSocket ws;
	private String[] array;
	Timer timer = new Timer();
	private String ip_Address = "192.168.1.6:8081"; // 10.0.2.2:8081

	private ArrayList<Game> scheduledGames = new ArrayList<Game>();
	private ArrayList<Game> finishedGames = new ArrayList<Game>();
	private ArrayList<Game> liveGames = new ArrayList<Game>();
	private ArrayList<Game> myGames = new ArrayList<Game>();
	
	private int tokens;
	private String deviceId;

	private Communicator() {
		int delay = 0; // delay time
		int period = 100; // repeat time

		try {
			ws = new WebSocket(new URI("ws://" + ip_Address));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		System.out.println("+++++++sssssssssssss");
		deviceId = User.getUser().getDeviceId();

		timer.scheduleAtFixedRate(new TimerTask() {

			public void run() {

				timerMethod();

			}

		}, delay, period);

	}

	public static Communicator getCommunicator() {
		if (comm == null)
			comm = new Communicator();
		return comm;
	}

	private void timerMethod() {
		try {
			while (true) {
				if (!ws.isConnected())
					continue;
				response = ws.recv();
				array = response.split("#");

				if (response.equals("") || array[0].equals("REFRESH")) {
					break;
				} else if (array[0].equals(deviceId))
					if (array[1].equals("Lista")) {
						if (array[2].equals("SCHEDULED")) {

							scheduledGames.clear();
							for (int i = 3; i < array.length; i++) {
								String[] array_zarez = array[i].split(",");

								Game game = getParsedGame(array_zarez);

								scheduledGames.add(game);
							}
							ws.close();
						} else if (array[2].equals("FINISHED")) {

							finishedGames.clear();
							for (int i = 3; i < array.length; i++) {
								String[] array_zarez = array[i].split(",");

								Game game = getParsedGame(array_zarez);

								finishedGames.add(game);
							}
							ws.close();
						} else if (array[2].equals("LIVE")) {

							liveGames.clear();
							for (int i = 3; i < array.length; i++) {
								String[] array_zarez = array[i].split(",");

								Game game = getParsedGame(array_zarez);

								liveGames.add(game);
							}
							ws.close();
						} else if (array[2].equals("MY")) {

							myGames.clear();
							for (int i = 3; i < array.length; i++) {
								String[] array_zarez = array[i].split(",");

								Game game = getParsedGame(array_zarez);

								myGames.add(game);
							}
							ws.close();
						}
					} else if (array[1].equals("Tokens")) {
						tokens = Integer.parseInt(array[2]);
						
						ws.close();
					} 
				System.out.println(array[0]);
				System.out.println(array[1]);
			}
		} catch (Exception e) {
			timer.cancel();
			e.printStackTrace();
		}
	}

	private Game getParsedGame(String[] array_zarez) {
		Game game = new Game();
		game.setGameId(Integer.parseInt(array_zarez[0]));
		game.setHomeTeam(array_zarez[1]);
		game.setAwayTeam(array_zarez[2]);
		game.setBeginTime(Long.parseLong(array_zarez[3]));
		game.setHomeScore(Integer.parseInt(array_zarez[4]));
		game.setAwayScore(Integer.parseInt(array_zarez[5]));
		game.setFinished(Integer.parseInt(array_zarez[6]) == 1);
		game.setTip(array_zarez[7].charAt(0));
		return game;
	}

	public ArrayList<Game> getScheduledGames() {
		try {
			ws.connect();
			request = "SELECT_LISTA#" + deviceId + "#SCHEDULED";
			ws.send(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (ws.isConnected())
			;

		return scheduledGames;

	};

	public ArrayList<Game> getFinishedGames() {
		try {
			ws.connect();
			request = "SELECT_LISTA#" + deviceId + "#FINISHED";
			ws.send(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (ws.isConnected())
			;

		return finishedGames;
	}

	public ArrayList<Game> getLiveGames() {
		liveGames.clear();
		try {
			ws.connect();
			request = "SELECT_LISTA#" + deviceId + "#LIVE";
			ws.send(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (ws.isConnected()) 
			;

		return liveGames;
	}

	public ArrayList<Game> getMyGames() {
		myGames.clear();
		try {
			ws.connect();
			request = "SELECT_LISTA#" + deviceId + "#MY";
			ws.send(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (ws.isConnected());

		return myGames;
	}

	public int getTokens() {
		try {
			ws.connect();
			request = "TOKEN_NUM#" + deviceId;
			ws.send(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (ws.isConnected());
		
		return tokens;
	}
	
	public void addTip(int gameId){
		try {
			ws.connect();
			request = "ADD_TIP#" + deviceId+"#"+gameId;
			ws.send(request);
			ws.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPayment(int tokenNum, int price, String id){
		try {
			ws.connect();
			request = "PAYMENT#" + deviceId+"#"+tokenNum+"#"+price+"#"+id;
			System.out.println(request);
			ws.send(request);
			ws.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
