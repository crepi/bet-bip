package com.infobip.campus.betbip;

import java.util.ArrayList;

import c.mpayments.android.PurchaseManager;
import c.mpayments.android.PurchaseRequest;

import com.infobip.campus.betbip.manager.GamesManager;
import com.infobip.campus.betbip.manager.MyPurchaseListener;
import com.infobip.campus.betbip.models.Game;
import com.infobip.campus.betbip.user.User;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class TabFragment extends Fragment {
	public static final String ARG_TAB_NUMBER = "tab_number";

	private ScoreListAdapter sla;


	public TabFragment() {
	}

	OnClickListener mPurchaseClick = new OnClickListener() {
		@Override
		public void onClick(final View v) {

			PurchaseManager.attachPurchaseListener(new MyPurchaseListener());

			PurchaseRequest pr = new PurchaseRequest(
					"005b99c8f310332b9763df5792ab27d9");
			pr.setOfflineModeEnabled(true); // optional
			pr.setTestModeEnabled(true);
			pr.setClientId(User.getUser().getDeviceId());
			PurchaseManager.startPurchase(pr, v.getContext());
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_tab, container,
				false);

		int i = getArguments().getInt(ARG_TAB_NUMBER);
		String tab = getResources().getStringArray(R.array.tabs_array)[i];

		ListView lv = (ListView) rootView.findViewById(R.id.list);

		ArrayList<Game> data;

		if (tab.equals("Scheduled")) {
			GamesManager.getManager().updateScheduledGames();
			data = GamesManager.getManager().getScheduledGames();
		} else if (tab.equals("Finished")) {
			GamesManager.getManager().updateFinishedGames();
			data = GamesManager.getManager().getFinishedGames();
		} else if (tab.equals("Live")) {
			GamesManager.getManager().updateLiveGames();
			data = GamesManager.getManager().getLiveGames();
		} else if (tab.equals("My Games")) {
			GamesManager.getManager().updateMyGames();
			data = GamesManager.getManager().getMyGames();
		} else {
			data = new ArrayList<Game>();
		}

		sla = new ScoreListAdapter(lv.getContext(), data, tab.charAt(0));
		lv.setAdapter(sla);

		getActivity().setTitle(tab);

		TextView tv = (TextView) rootView.findViewById(R.id.tokens);

		tv.setText(User.getUser().getTokens() + "");

		// payment
		((Button) rootView.findViewById(R.id.buy_tokens))
				.setOnClickListener(mPurchaseClick);

		return rootView;
	}
}
