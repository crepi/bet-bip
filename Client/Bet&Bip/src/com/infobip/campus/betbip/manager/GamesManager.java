package com.infobip.campus.betbip.manager;

import java.util.ArrayList;




import com.infobip.campus.betbip.communication.Communicator;
import com.infobip.campus.betbip.models.Game;
import com.infobip.campus.betbip.user.User;

public class GamesManager {
	private static GamesManager manager = null;
	
	private ArrayList<Game> scheduledGames;
	private ArrayList<Game> liveGames;
	private ArrayList<Game> finishedGames;
	private ArrayList<Game> myGames;
	
	private GamesManager(){
		scheduledGames = Communicator.getCommunicator().getScheduledGames();
		finishedGames = Communicator.getCommunicator().getFinishedGames();
		liveGames = Communicator.getCommunicator().getLiveGames();
		myGames = Communicator.getCommunicator().getMyGames();
	}
	
	public static GamesManager getManager() {
		if (manager == null)
			manager = new GamesManager();
		return manager;
	}
	
	public void getTip(Game g){
		scheduledGames.remove(g); 
		myGames.add(g);
		User.getUser().decreaseTokens();
		Communicator.getCommunicator().addTip(g.getGameId());
	}
	
	public void updateLiveGames() {
		liveGames.clear();
		myGames.clear();
		liveGames = Communicator.getCommunicator().getLiveGames();
		myGames = Communicator.getCommunicator().getMyGames();
	}
	
	public void updateMyGames() {
		myGames.clear();
		myGames = Communicator.getCommunicator().getMyGames();
	}
	
	public void updateScheduledGames() {
		scheduledGames.clear();
		scheduledGames = Communicator.getCommunicator().getScheduledGames();
	}
	
	public void updateFinishedGames() {
		finishedGames.clear();
		myGames = Communicator.getCommunicator().getFinishedGames();
	}
	
//	public void addScheduledGame(Game g){
//		scheduledGames.add(g);
//	}
//	
//	public void addLiveGame(Game g){
//		liveGames.add(g);
//	}
//	
//	public void addFinishedGame(Game g){
//		finishedGames.add(g);
//	}
//	
//	public void addMyGame(Game g){
//		myGames.add(g);
//	}

	public ArrayList<Game> getScheduledGames() {
		return scheduledGames;
	}

	public void setScheduledGames(ArrayList<Game> scheduledGames) {
		this.scheduledGames = scheduledGames;
	}
	
	public ArrayList<Game> getLiveGames() {
		return liveGames;
	}

	public void setLiveGames(ArrayList<Game> liveGames) {
		this.liveGames = liveGames;
	}

	public ArrayList<Game> getFinishedGames() {
		return finishedGames;
	}

	public void setFinishedGames(ArrayList<Game> finishedGames) {
		this.finishedGames = finishedGames;
	}

	public ArrayList<Game> getMyGames() {
		return myGames;
	}

	public void setMyGames(ArrayList<Game> myGames) {
		this.myGames = myGames;
	}
}
