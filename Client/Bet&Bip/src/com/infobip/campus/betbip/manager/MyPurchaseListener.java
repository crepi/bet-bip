package com.infobip.campus.betbip.manager;

import com.infobip.campus.betbip.communication.Communicator;
import com.infobip.campus.betbip.user.User;

import c.mpayments.android.PurchaseListener;
import c.mpayments.android.PurchaseResponse;

public class MyPurchaseListener implements PurchaseListener {
	
	@Override
	public void onPurchaseSuccess(PurchaseResponse paramPurchaseResponse) {
		// handle purchase success
		Communicator.getCommunicator().addPayment(
				paramPurchaseResponse.getItemAmount(),
				(int) paramPurchaseResponse.getPrice(),
				paramPurchaseResponse.getTransactionId());
		User.getUser().setTokens(
				User.getUser().getTokens()
						+ paramPurchaseResponse.getItemAmount());
	} 

	@Override
	public void onPurchasePending(PurchaseResponse paramPurchaseResponse) {
		// notification that purchase verification
		// has begun
	}

	@Override
	public void onPurchaseFailed(PurchaseResponse paramPurchaseResponse) {
		// purchase fail
	}

	@Override
	public void onPurchaseCanceled(PurchaseResponse paramPurchaseResponse) {
		// purchase canceled by user
	}

}
