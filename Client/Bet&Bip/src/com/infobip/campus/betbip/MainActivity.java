package com.infobip.campus.betbip;

//import com.infobip.push.AbstractPushReceiver;
//import com.infobip.push.PushNotification;
//import com.infobip.push.PushNotificationManager;

import com.infobip.campus.betbip.user.User;

import c.mpayments.android.BillingActivity;
import c.mpayments.android.util.Logger;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.IntentFilter;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mTabTitles;

	// Declare push receiver
//	private BroadcastReceiver receiver = new AbstractPushReceiver() {
//
//		@Override
//		public void onRegistered(Context context) {
//		}
//
//		@Override
//		public void onUnregistered(Context context) {
//		}
//
//		@Override
//		public void onNotificationReceived(PushNotification notification,
//				Context context) {
//		}
//
//		@Override
//		public void onError(int reason, Context context) {
//		}
//
//	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = tMgr.getLine1Number();
		
		System.out.println("*******device "+deviceId);

		User.getUser().setDeviceId(deviceId); 
		
		mTabTitles = getResources().getStringArray(R.array.tabs_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mTabTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		BillingActivity.mcc = "219";
		BillingActivity.mnc = "01";

		Logger.setDebugModeEnabled(true);
		if (savedInstanceState == null) {
			selectItem(0);
		}

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();

		StrictMode.setThreadPolicy(policy);
		
//		PushNotificationManager manager = new PushNotificationManager(this);
//	    manager.initialize(manager.getDeviceId(), "<applicationId>", "<applicationSecret>");
	}
	
//	@Override
//	protected void onResume() {
//	    super.onResume();
//
//	    IntentFilter filter = new IntentFilter();
//	    filter.addAction("com.infobip.push.intent.REGISTERED_FOR_NOTIFICATIONS");
//	    filter.addAction("com.infobip.push.intent.REGISTRATION_REFRESHED");
//	    filter.addAction("com.infobip.push.intent.UNREGISTERED_FROM_NOTIFICATIONS");
//	    filter.addAction("com.infobip.push.intent.NOTIFICATION_RECEIVED");
//	    filter.addAction("com.infobip.push.intent.NOTIFICATION_OPENED");
//	    filter.addAction("com.infobip.push.intent.ERROR");
//	    filter.addCategory(getPackageName());
//
//	    registerReceiver(receiver, filter); //Register receiver
//	}
//
//	@Override
//	protected void onPause() {
//	    super.onPause();
//	    unregisterReceiver(receiver); //Unregister receiver
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = new TabFragment();
		Bundle args = new Bundle();
		args.putInt(TabFragment.ARG_TAB_NUMBER, position);
		fragment.setArguments(args);

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(mTabTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
}
