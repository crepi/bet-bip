package com.infobip.campus.betbip;

import java.util.ArrayList;

import c.mpayments.android.PurchaseManager;
import c.mpayments.android.PurchaseRequest;

import com.infobip.campus.betbip.manager.GamesManager;
import com.infobip.campus.betbip.manager.MyPurchaseListener;
import com.infobip.campus.betbip.models.Game;
import com.infobip.campus.betbip.user.User;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ScoreListAdapter extends BaseAdapter {

	private ArrayList<Game> listData;

	private LayoutInflater layoutInflater;

	private char status;

	public ScoreListAdapter(Context context, ArrayList<Game> listData,
			char status) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
		this.setStatus(status);
	}

	public void setListData(ArrayList<Game> listData) {
		this.listData = listData;
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int arg0) {
		return listData.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.scheduled_list_item,
					null);
			holder = new ViewHolder();
			holder.homeTeam = (TextView) convertView
					.findViewById(R.id.homeTeam);
			holder.awayTeam = (TextView) convertView
					.findViewById(R.id.awayTeam);
			holder.homeScore = (TextView) convertView
					.findViewById(R.id.homeScore);
			holder.awayScore = (TextView) convertView
					.findViewById(R.id.awayScore);
			holder.btnTip = (Button) convertView.findViewById(R.id.gettip);
			holder.gameTip = (TextView) convertView.findViewById(R.id.gameTip);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.homeTeam.setText(listData.get(position).getHomeTeam());
		holder.awayTeam.setText(listData.get(position).getAwayTeam());
		holder.homeScore.setText(listData.get(position).getHomeScore() + "");
		holder.awayScore.setText(listData.get(position).getAwayScore() + "");
		holder.btnTip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (User.getUser().getTokens() < User.TIP_PRICE) {
					PurchaseManager
							.attachPurchaseListener(new MyPurchaseListener());

					PurchaseRequest pr = new PurchaseRequest(
							"005b99c8f310332b9763df5792ab27d9");
					pr.setOfflineModeEnabled(true); // optional
					pr.setTestModeEnabled(true);
					pr.setClientId(User.getUser().getDeviceId());
					PurchaseManager.startPurchase(pr, v.getContext());
				} else {
					GamesManager.getManager().getTip(listData.get(position));
					ScoreListAdapter.this.notifyDataSetChanged();
				}
			}
		});

		holder.btnTip.setVisibility(status == 'S' ? View.VISIBLE
				: View.INVISIBLE);
		if (status == 'M') {
			holder.gameTip.setText("tip " + listData.get(position).getTip());
		}
		if (listData.get(position).isFinished()) {
			int res = listData.get(position).getHomeScore()
					- listData.get(position).getAwayScore();
			char score, tipped = listData.get(position).getTip();

			if (res > 0)
				score = '1';
			else if (res < 0)
				score = '2';
			else
				score = 'x';

			if (score == tipped) {
				holder.homeScore.setTextColor(Color.GREEN);
				holder.awayScore.setTextColor(Color.GREEN);
			} else {
				holder.homeScore.setTextColor(Color.RED);
				holder.awayScore.setTextColor(Color.RED);
			}
		}
		return convertView;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	static class ViewHolder {
		TextView homeTeam;
		TextView awayTeam;
		TextView homeScore;
		TextView awayScore;
		Button btnTip;
		TextView gameTip;
	}

}
