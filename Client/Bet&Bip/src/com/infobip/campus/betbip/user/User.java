package com.infobip.campus.betbip.user;

import com.infobip.campus.betbip.communication.Communicator;


public class User {
	public static final int TIP_PRICE = 5;
	
	private static User user = null;
	
	private String deviceId;
	private int tokens=-1;
	
	private User(){
		//tokens = Communicator.getCommunicator().getTokens();
	}
	
	public static User getUser(){
		if (user == null)
			user = new User();
		return user;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public int getTokens() {
		if (tokens == -1)
			tokens = Communicator.getCommunicator().getTokens();
		return tokens;
	}

	public void setTokens(int tokens) {
		this.tokens = tokens;
	}

	public void decreaseTokens() {
		tokens -= TIP_PRICE;
	}
}
